import { NextFunction, Request, Response } from "express";
import { getUser, loginUser, registerUser } from "../logic/auth.logic";

const AuthController = {
  register: async (req: Request, res: Response, next: NextFunction) => {
    try {
      const { email, password, displayName } = req.body;

      const user = await registerUser({ email, password, displayName });

      res.json({
        success: true,
        data: {
          data: user,
        },
        message: "User registered successfully",
      });
    } catch (error) {
      next(error);
    }
  },
  login: async (req: Request, res: Response, next: NextFunction) => {
    try {
      const { email, password } = req.body;

      const token = await loginUser({ email, password });

      res.json({
        success: true,
        token,
        message: "Logged in successfully",
      });
    } catch (error) {
      next(error);
    }
  },
  getSelf: async (req: Request, res: Response, next: NextFunction) => {
    try {
      const userId = req?.user?._id;

      const user = await getUser(userId);

      res.json({
        success: true,
        data: { data: user },
        message: "User data fetched successfully",
      });
    } catch (error) {
      next(error);
    }
  },
};

export default AuthController;
