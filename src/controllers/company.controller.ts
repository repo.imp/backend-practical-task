import { NextFunction, Request, Response } from "express";
import {
  createCompany,
  deleteCompany,
  getAllCompany,
  updateCompany,
} from "../logic/company.logic";

const companyController = {
  createCompany: async (req: Request, res: Response, next: NextFunction) => {
    try {
      const createdBy = req?.user?._id;

      const { code, name, link, machine, frequency, contactNumber, remark } =
        req.body;
      await createCompany({
        code,
        name,
        link,
        machine,
        frequency,
        contactNumber,
        remark,
        createdBy,
      });
      res.json({
        success: true,
        message: "Company created successfully",
      });
    } catch (error) {
      next(error);
    }
  },
  updateCompany: async (req: Request, res: Response, next: NextFunction) => {
    try {
      const createdBy = req?.user?._id;

      const id = req?.params?.companyId;

      const { code, name, link, machine, frequency, contactNumber, remark } =
        req.body;
      await updateCompany({
        code,
        name,
        link,
        machine,
        frequency,
        contactNumber,
        remark,
        createdBy,
        id,
      });
      res.json({
        success: true,
        message: "Company updated successfully",
      });
    } catch (error) {
      next(error);
    }
  },

  deleteCompany: async (req: Request, res: Response, next: NextFunction) => {
    try {
      const createdBy = req?.user?._id;

      const id = req?.params?.companyId;
      await deleteCompany({
        id,
        createdBy,
      });
      res.json({
        success: true,
        message: "Company deleted successfully",
      });
    } catch (error) {
      next(error);
    }
  },
  getAllCompany: async (req: Request, res: Response, next: NextFunction) => {
    try {
      const createdBy = req?.user?._id;

      const { perPage, pageNo, searchTitle } = req.query;
      const [companyData, totalData] = await getAllCompany({
        perPage: perPage ? Number(perPage) : undefined,
        pageNo: pageNo ? Number(pageNo) : undefined,
        createdBy,
        searchTitle: searchTitle ? String(searchTitle) : undefined,
      });
      res.json({
        success: true,
        message: "Companies data fetched successfully",
        data: {
          data: companyData,
          totalData: totalData,
        },
      });
    } catch (error) {
      next(error);
    }
  },
};

export default companyController;
