import { Document, ObjectId } from "mongoose";

export interface ICompany extends Document {
  code: string;
  name: string;
  link: string;
  machine: number;
  frequency: "LOW" | "MEDIUM" | "HIGH";
  contactNumber: string;
  remark: string;
  createdBy: ObjectId;
}
