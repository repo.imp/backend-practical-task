import { Document } from "mongoose";

export interface IUser extends Document {
  displayName: string;
  email: string;
  password: string;
  token: string;
  verificationInfo: {
    code: string;
    expireAt: Date;
  };
}
