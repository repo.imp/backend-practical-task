import bcrypt from "bcrypt";

export const hashString = async (string: string) => {
  const hashPassword = await bcrypt.hash(string, 10);
  return hashPassword;
};

export const compareHash = async (string: string, hash: string) => {
  const isMatch = await bcrypt.compare(string, hash);
  return isMatch;
};
