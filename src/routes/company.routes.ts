import express from "express";
import companyController from "../controllers/company.controller";
import { isAuthenticated } from "../middlewares/authenticated.middleware";
import validate from "../middlewares/validation.middleware";
import {
  companyCreateValidation,
  companyDeleteValidation,
  companyGetAllValidation,
  companyUpdateValidation,
} from "../validations/company.validations";

const router = express.Router();

router.post(
  "/",
  companyCreateValidation,
  validate,
  isAuthenticated,
  companyController.createCompany
);
router.patch(
  "/:companyId",
  companyUpdateValidation,
  validate,
  isAuthenticated,
  companyController.updateCompany
);
router.delete(
  "/:companyId",
  companyDeleteValidation,
  validate,
  isAuthenticated,
  companyController.deleteCompany
);
router.get(
  "/",
  companyGetAllValidation,
  validate,
  isAuthenticated,
  companyController.getAllCompany
);

export default router;
