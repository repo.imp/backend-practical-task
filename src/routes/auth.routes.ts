import express from "express";
import AuthController from "../controllers/auth.controllers";
import { isAuthenticated } from "../middlewares/authenticated.middleware";
import validate from "../middlewares/validation.middleware";
import {
  loginValidation,
  registerValidation,
} from "../validations/auth.validations";

const router = express.Router();

router.post("/register", registerValidation, validate, AuthController.register);
router.post("/login", loginValidation, validate, AuthController.login);
router.get("/self", isAuthenticated, AuthController.getSelf);

export default router;
