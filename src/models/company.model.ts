import { Model, Schema, model } from "mongoose";
import { ICompany } from "../types/company";

const companySchema = new Schema<ICompany, Model<ICompany>>({
  code: {
    type: String,
    unique: true,
  },
  name: {
    type: String,
    required: true,
  },
  link: {
    type: String,
    required: true,
  },
  machine: {
    type: Number,
    required: true,
  },
  frequency: {
    type: String,
    enum: ["LOW", "MEDIUM", "HIGH"],
    default: "LOW",
  },
  contactNumber: String,
  remark: String,
  createdBy: {
    type: Schema.Types.ObjectId,
    ref: "User",
  },
});

export const CompanyModel = model<ICompany, Model<ICompany>>(
  "Company",
  companySchema
);
