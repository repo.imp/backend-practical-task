import { Model, Schema, model } from "mongoose";
import { IUser } from "../types/user";

const userSchema = new Schema<IUser, Model<IUser>>({
  displayName: {
    type: String,
    required: true,
  },
  email: {
    type: String,
    required: true,
    unique: true,
  },
  password: String,
  token: String,
  verificationInfo: {
    code: String,
    expireAt: Date,
  },
});

export const UserModel = model<IUser, Model<IUser>>("User", userSchema);
