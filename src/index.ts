import express from "express";
import errorHandler from "./middlewares/error.middleware";
import routeParser from "./middlewares/routeParser.middleware";
import setupMiddlewares from "./middlewares/top.middleware";
import connectToDb from "./services/db.services";
require("dotenv").config();

const PORT = process.env.PORT || 8000;

//initialize the app

const app = express();
connectToDb();

//set up all the primary middlewares
setupMiddlewares(app);

//set up auth routes
routeParser(app);

//error handler
errorHandler(app);

app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}`);
});
