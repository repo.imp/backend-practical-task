import { NextFunction, Request, Response } from "express";
import { Unauthorized } from "http-errors";
import { verifyToken } from "../services/jwt.services";

declare global {
  namespace Express {
    interface Request {
      user: {
        _id: string;
        displayName: string;
        email: string;
      };
    }
  }
}

export const isAuthenticated = (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  try {
    if (!req?.headers?.authorization)
      throw new Unauthorized("No token provided");
    const token = req.headers.authorization?.split(" ")?.[1];
    if (!token) throw new Unauthorized("No token provided");

    const user = verifyToken(token);
    if (!user) throw new Unauthorized("Unauthorized access");

    req.user = user as any;
    next();
  } catch (error) {
    res.status(401).json({
      success: false,
      msg: error instanceof Error ? error?.message : "Unauthorized access",
    });
  }
};
