import { NextFunction, Request, Response } from "express";
import { validationResult } from "express-validator";

export default function validate(
  req: Request,
  res: Response,
  next: NextFunction
) {
  try {
    const error = validationResult(req);

    if (!error.isEmpty()) {
      return res.status(400).json({
        success: false,
        message: error
          .array()
          ?.map((item) => item?.msg)
          ?.join(" and "),
      });
    }
    next();
  } catch (error) {
    res.status(500).json({
      success: false,
      message: "Validation failed",
    });
  }
}
