import { body, param, query } from "express-validator";

export const companyCreateValidation = [
  body("code").notEmpty().withMessage("Code is required"),
  body("name").notEmpty().withMessage("Name is required"),
  body("link").notEmpty().withMessage("Link is required"),
  body("machine")
    .notEmpty()
    .withMessage("Machine is required")
    .isNumeric()
    .withMessage("Machine must be a number")
    .toInt(),
  body("frequency")
    .notEmpty()
    .withMessage("Frequency is required")
    .isIn(["LOW", "MEDIUM", "HIGH"])
    .withMessage("Frequency must be one of LOW, MEDIUM, HIGH"),
  body("contactNumber").notEmpty().withMessage("Contact Number is required"),
  body("remark").notEmpty().withMessage("Remark is required"),
];
export const companyUpdateValidation = [
  param("companyId").notEmpty().withMessage("Company Id is required"),
  body("machine")
    .optional()
    .isNumeric()
    .withMessage("Machine must be a number")
    .toInt(),
  body("frequency")
    .optional()
    .isIn(["LOW", "MEDIUM", "HIGH", ""])
    .withMessage("Frequency must be one of LOW, MEDIUM, HIGH"),
];
export const companyDeleteValidation = [
  param("companyId").notEmpty().withMessage("Company Id is required"),
];
export const companyGetAllValidation = [
  query("perPage")
    .optional()
    .isNumeric()
    .withMessage("perPage must be a number")
    .toInt(),
  query("pageNo")
    .optional()
    .isNumeric()
    .withMessage("pageNo must be a number")
    .toInt(),
];
