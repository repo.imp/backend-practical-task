import { InternalServerError, Unauthorized } from "http-errors";
import { UserModel } from "../models/user.model";
import { compareHash, hashString } from "../services/hash.services";
import { generateToken } from "../services/jwt.services";

export const registerUser = async ({
  displayName,
  email,
  password,
}: {
  displayName: string;
  email: string;
  password: string;
}) => {
  try {
    //first the password
    const hashPassword = await hashString(password);

    const user = await UserModel.create({
      displayName,
      email,
      password: hashPassword,
    });
    if (!user) throw new Error("User not created");
    return user._id;
  } catch (error) {
    throw error;
  }
};
export const loginUser = async ({
  email,
  password,
}: {
  email: string;
  password: string;
}) => {
  try {
    //check if user exist with the email

    const user = await UserModel.findOne({ email });

    if (!user) throw new Unauthorized("User not found. Please register");

    //compare the password

    const isMatch = await compareHash(password, user.password);

    if (!isMatch)
      throw new Unauthorized("Incorrect credentials. Please try again!");

    //create a token

    const token = generateToken({
      email: user.email,
      _id: user._id,
      displayName: user.displayName,
    });

    if (!token) throw new InternalServerError("Token not created");

    return token;
  } catch (error) {
    throw error;
  }
};
export const getUser = async (id: string) => {
  try {
    //check if user exist with the email

    const user = await UserModel.findById(id);

    if (!user) throw new Unauthorized("User not found. Please register");

    return user;
  } catch (error) {
    throw error;
  }
};
