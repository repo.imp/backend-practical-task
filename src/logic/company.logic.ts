import { BadRequest, NotFound } from "http-errors";
import { FilterQuery } from "mongoose";
import { CompanyModel } from "../models/company.model";
import { ICompany } from "../types/company";

export const createCompany = async ({
  code,
  name,
  link,
  machine,
  frequency,
  contactNumber,
  remark,
  createdBy,
}: {
  code: string;
  name: string;
  link: string;
  machine: number;
  frequency: string;
  contactNumber: string;
  remark: string;
  createdBy: string;
}) => {
  try {
    const createCompany = await CompanyModel.create({
      code,
      name,
      link,
      machine,
      frequency,
      contactNumber,
      remark,
      createdBy,
    });
    if (!createCompany) throw new BadRequest("Company not created");
    return createCompany.id;
  } catch (error) {
    throw error;
  }
};
export const updateCompany = async ({
  code,
  name,
  link,
  machine,
  frequency,
  contactNumber,
  remark,
  createdBy,
  id,
}: {
  code?: string;
  name?: string;
  link?: string;
  machine?: number;
  frequency?: string;
  contactNumber?: string;
  remark?: string;
  createdBy?: string;
  id: string;
}) => {
  try {
    const createCompany = await CompanyModel.findOneAndUpdate(
      { _id: id, createdBy },
      {
        code,
        name,
        link,
        machine,
        frequency,
        contactNumber,
        remark,
        createdBy,
      }
    );
    if (!createCompany) throw new NotFound("Company not found");
    return createCompany.id;
  } catch (error) {
    throw error;
  }
};
export const deleteCompany = async ({
  createdBy,
  id,
}: {
  createdBy?: string;
  id: string;
}) => {
  try {
    const createCompany = await CompanyModel.findOneAndDelete({
      _id: id,
      createdBy,
    });
    if (!createCompany) throw new NotFound("Company not found");
    return createCompany.id;
  } catch (error) {
    throw error;
  }
};
export const getAllCompany = async ({
  createdBy,
  perPage = 10,
  pageNo = 1,
  searchTitle,
}: {
  createdBy?: string;
  perPage?: number;
  pageNo?: number;
  searchTitle?: string;
}) => {
  try {
    let dynamicQuery: FilterQuery<ICompany> = {};

    if (searchTitle)
      dynamicQuery.$or = [
        { code: new RegExp(searchTitle, "i") },
        { name: new RegExp(searchTitle, "i") },
        { link: new RegExp(searchTitle, "i") },
        { frequency: new RegExp(searchTitle, "i") },
        { contactNumber: new RegExp(searchTitle, "i") },
        { remark: new RegExp(searchTitle, "i") },
      ];

    const [companyData, totalData] = await Promise.all([
      CompanyModel.find({
        createdBy,
        ...dynamicQuery,
      })
        .select("-__v")
        .limit(perPage)
        .skip(perPage * (pageNo - 1))
        .lean(),
      CompanyModel.find({
        createdBy,
        ...dynamicQuery,
      }).countDocuments(),
    ]);

    return [companyData, totalData];
  } catch (error) {
    throw error;
  }
};
